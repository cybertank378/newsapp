package io.rahman.newsapp.network.repository

import androidx.lifecycle.LiveData
import io.rahman.newsapp.data.model.NewsArticle
import io.rahman.newsapp.data.model.NewsResponse
import io.rahman.newsapp.state.NetworkState

interface INewsRepository {
    suspend fun getNews(countryCode: String, pageNumber: Int): NetworkState<NewsResponse>

    suspend fun searchNews(searchQuery: String, pageNumber: Int): NetworkState<NewsResponse>

    suspend fun saveNews(news: NewsArticle): Long

    fun getSavedNews(): LiveData<List<NewsArticle>>

    suspend fun deleteNews(news: NewsArticle)

    suspend fun deleteAllNews()
}
