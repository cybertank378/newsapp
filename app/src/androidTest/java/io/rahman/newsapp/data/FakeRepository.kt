package io.rahman.newsapp.data

import FakeDataUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.rahman.newsapp.data.model.NewsArticle
import io.rahman.newsapp.data.model.NewsResponse
import io.rahman.newsapp.network.repository.INewsRepository
import io.rahman.newsapp.state.NetworkState

class FakeRepository : INewsRepository {

    private val observableNewsArticle = MutableLiveData<List<NewsArticle>>()

    override suspend fun getNews(
        countryCode: String,
        pageNumber: Int
    ): NetworkState<NewsResponse> {
    }

    override suspend fun searchNews(
        searchQuery: String,
        pageNumber: Int
    ): NetworkState<NewsResponse> {
    }

    override suspend fun saveNews(news: NewsArticle): Long {
    }

    override fun getSavedNews(): LiveData<List<NewsArticle>> {
        return FakeDataUtil.getFakeNewsArticleLiveData()
    }

    override suspend fun deleteNews(news: NewsArticle) {
    }

    override suspend fun deleteAllNews() {
    }
}