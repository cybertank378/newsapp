package io.rahman.newsapp.network.api

import io.rahman.newsapp.BuildConfig
import io.rahman.newsapp.data.model.NewsResponse
import io.rahman.newsapp.utils.Constants.Companion.DEFAULT_PAGE_INDEX

import io.rahman.newsapp.utils.Constants.Companion.QUERY_PER_PAGE
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("top-headlines")
    suspend fun getNews(
        @Query("country")
        countryCode: String = "us",
        @Query("page")
        pageNumber: Int = DEFAULT_PAGE_INDEX,
        @Query("pageSize")
        pageSize: Int = QUERY_PER_PAGE,
        @Query("apiKey")
        apiKey: String = BuildConfig.GRADLE_API_KEY
    ): Response<NewsResponse>

    @GET("everything")
    suspend fun searchNews(
        @Query("q")
        searchQuery: String,
        @Query("page")
        pageNumber: Int = 1,
        @Query("pageSize")
        pageSize: Int = QUERY_PER_PAGE,
        @Query("apiKey")
        apiKey: String = BuildConfig.GRADLE_API_KEY
    ): Response<NewsResponse>
}