package io.rahman.newsapp.utils

class Constants {
    companion object {
        const val searchTimeDelay = 500L
        const val QUERY_PER_PAGE = 10
        const val DEFAULT_PAGE_INDEX = 1
    }
}